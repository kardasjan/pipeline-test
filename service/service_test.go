package service

import "testing"

func TestTheAnswer(t *testing.T) {
	got := TheAnswer()
    if got != 42 {
        t.Errorf("Should be 42 but got: %d", got)
    }
}
