FROM golang:latest AS builder

RUN mkdir -p /go/src/app
WORKDIR /go/src/app

COPY . ./
RUN CGO_ENABLED=0 GOOS=linux go build -a -installsuffix nocgo -o /app .

FROM alpine:latest

COPY --from=builder /app ./
ENTRYPOINT ["./app"]
